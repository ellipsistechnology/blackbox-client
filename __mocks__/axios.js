let testData = {};
let testOptionsData = {};
let testOptionsHeaders = {};

module.exports.default = {
  get: jest.fn((path, options) => {
    if(options.params) {
      return Promise.resolve({
        data: testData[path].filter(d => d.name === options.params.q),
        headers:testOptionsHeaders[path]
      })
    } else {
      return Promise.resolve({
        data: testData[path],
        headers:testOptionsHeaders[path]
      })
    }
  }),
  post: jest.fn((path, body) => {
    testData[path+'/'+body.name] = body
    return Promise.resolve({data:{name:body.name}})
  }),
  put: jest.fn((path, body) => {
    testData[path] = body
    return Promise.resolve({data:{name:body.name}})
  }),
  patch: jest.fn((path, body) => {
    Object.assign(testData[path], body)
    return Promise.resolve({data:{name:body.name || /([^\/]*)\/?$/.exec(path)[1]}})
  }),
  delete: jest.fn((path) => {
    delete testData[path]
    return Promise.resolve({data:{name:/([^\/]*)\/?$/.exec(path)[1]}})
  }),
  options: jest.fn((path) => {
    return Promise.resolve({
      data:testOptionsData[path],
      headers:testOptionsHeaders[path]
    })
  }),
  __setup__: (data, optionsData, optionsHeaders) => {
    testData = data
    testOptionsData = optionsData
    testOptionsHeaders = optionsHeaders
  }
};
