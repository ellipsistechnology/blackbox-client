"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var __1 = require("..");
var axios_1 = require("axios");
var HOST = 'http://dummy.com';
var RULEBASE = 'http://dummy.com/rulebase';
var VALUES = 'http://dummy.com/rulebase/values';
var HOST_MISSING_DESCRIPTION = "desc-missing";
var HOST_MISSING_BBVERSION = "bbver-missing";
var HOST_MISSING_LINKS = "links-missing";
// Dummy service data (see __mocks__/axios.js:
var data = {};
data[HOST] = data[HOST + '/'] = {
    description: 'blah',
    bbversion: '1.2.3',
    links: {
        self: {
            href: "/"
        },
        rulebase: {
            href: "/rulebase",
            description: "The blackbox rulebase service.",
            types: [
                {
                    uri: "http://ellipsistechnology.com/schemas/blackbox/",
                    name: "rule"
                },
                {
                    uri: "http://ellipsistechnology.com/schemas/blackbox/",
                    name: "condition"
                },
                {
                    uri: "http://ellipsistechnology.com/schemas/blackbox/",
                    name: "value"
                }
            ]
        },
        test: {
            href: "/test",
            description: "Test.",
            types: [
                {
                    uri: "http://ellipsistechnology.com/schemas/blackbox/",
                    name: "test"
                }
            ]
        }
    }
};
data[RULEBASE] = data[RULEBASE + '/'] = {
    "description": "Blackbox rule-base service.",
    "bbversion": "1.0.0",
    "links": {
        "self": {
            "href": "/"
        },
        "rules": {
            "href": "/rulebase/rules",
            "description": "",
            "types": [
                {
                    "uri": "http://ellipsistechnology.com/schemas/blackbox/",
                    "name": "rule",
                    "format": "openapi:3.0:Schema Object"
                }
            ]
        },
        "conditions": {
            "href": "/rulebase/conditions",
            "description": "",
            "types": [
                {
                    "uri": "http://ellipsistechnology.com/schemas/blackbox/",
                    "name": "condition",
                    "format": "openapi:3.0:Schema Object"
                }
            ]
        },
        "values": {
            "href": "/rulebase/values",
            "description": "",
            "types": [
                {
                    "uri": "http://ellipsistechnology.com/schemas/blackbox/",
                    "name": "value",
                    "format": "openapi:3.0:Schema Object"
                }
            ]
        }
    }
};
var value1a = {
    name: 'bob',
    type: 'remote'
};
var value1b = {
    name: 'bill',
    type: 'remote'
};
var value2 = {
    name: 'bob',
    type: 'remote'
};
var value3 = {
    name: 'bob',
    type: 'some other type'
};
data[VALUES] = data[VALUES + '/'] = [value1a, value1b];
data[VALUES + '/' + value1a.name] = value1a;
data[VALUES + '/' + value1b.name] = value1b;
data[HOST_MISSING_LINKS] = {
    description: 'blah',
    bbversion: '1.2.3'
};
data[HOST_MISSING_DESCRIPTION] = {
    bbversion: '1.2.3',
    links: {}
};
data[HOST_MISSING_BBVERSION] = {
    description: 'blah',
    links: {}
};
var optionsData = {};
optionsData[HOST] = optionsData[HOST + '/'] = {};
var optionsHeaders = {};
optionsHeaders[HOST] = optionsHeaders[HOST + '/'] = {
    'Content-Blackbox-Type': 'service'
};
optionsHeaders[RULEBASE] = optionsHeaders[RULEBASE + '/'] = {
    'Content-Blackbox-Type': 'service'
};
optionsHeaders[VALUES] = optionsHeaders[VALUES + '/'] = {
    'Content-Blackbox-Type': 'http://ellipsistechnology.com/schemas/blackbox/value'
};
axios_1.default.__setup__(data, optionsData, optionsHeaders);
test("Invalid host response throws error.", function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, expect(new __1.ServiceClient("doesn't exist").init()).rejects.toBeTruthy()];
            case 1:
                _a.sent();
                return [4 /*yield*/, expect(new __1.ServiceClient(HOST_MISSING_LINKS).init()).rejects.toBeTruthy()];
            case 2:
                _a.sent();
                return [4 /*yield*/, expect(new __1.ServiceClient(HOST_MISSING_DESCRIPTION).init()).rejects.toBeTruthy()];
            case 3:
                _a.sent();
                return [4 /*yield*/, expect(new __1.ServiceClient(HOST_MISSING_BBVERSION).init()).rejects.toBeTruthy()];
            case 4:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
test("Root node returns a service.", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, new __1.ServiceClient(HOST).init()];
            case 1:
                client = _a.sent();
                expect(client.url).toEqual(HOST + '/');
                expect(client.service).toEqual(data[HOST]);
                return [2 /*return*/];
        }
    });
}); });
// TODO test services linked by absolute URL
test("Can navigate to child service.", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, ruleBaseTypes, allTypes;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, new __1.ServiceClient(HOST).init()];
            case 1:
                client = _a.sent();
                ruleBaseTypes = client.types('rulebase');
                expect(ruleBaseTypes).toEqual(data[HOST].links.rulebase.types);
                allTypes = client.types();
                expect(allTypes).toEqual(data[HOST].links.rulebase.types.concat(data[HOST].links.test.types));
                return [4 /*yield*/, expect(client.navigate({ service: 'rulebase' })
                        .then(function (rulebaseClient) {
                        expect(rulebaseClient.service).toEqual(data[RULEBASE]);
                        return client.navigate({ type: {
                                uri: 'http://ellipsistechnology.com/schemas/blackbox/',
                                name: 'value',
                                format: ''
                            } });
                    })
                        .then(function (valuesClient) { return valuesClient.get(); })
                        .then(function (response) { return expect(response).toEqual(data[VALUES]); })).resolves.toBeUndefined()];
            case 2:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
test("Can get data from service by name.", function () { return __awaiter(void 0, void 0, void 0, function () {
    var root, rulebase, values, v1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, new __1.ServiceClient(HOST).init()];
            case 1:
                root = _a.sent();
                return [4 /*yield*/, root.navigate({ service: 'rulebase' })];
            case 2:
                rulebase = _a.sent();
                expect(rulebase).toBeInstanceOf(__1.ServiceClient);
                return [4 /*yield*/, rulebase.navigate({ service: 'values' })];
            case 3:
                values = _a.sent();
                expect(values).toBeInstanceOf(__1.DataClient);
                return [4 /*yield*/, values.get(value1a.name)];
            case 4:
                v1 = _a.sent();
                expect(v1).toEqual(value1a);
                return [2 /*return*/];
        }
    });
}); });
test("Can get data with query.", function () { return __awaiter(void 0, void 0, void 0, function () {
    var root, rulebase, values, v1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, new __1.ServiceClient(HOST).init()];
            case 1:
                root = _a.sent();
                return [4 /*yield*/, root.navigate({ service: 'rulebase' })];
            case 2:
                rulebase = _a.sent();
                expect(rulebase).toBeInstanceOf(__1.ServiceClient);
                return [4 /*yield*/, rulebase.navigate({ service: 'values' })];
            case 3:
                values = _a.sent();
                expect(values).toBeInstanceOf(__1.DataClient);
                return [4 /*yield*/, values.query("bill")];
            case 4:
                v1 = _a.sent();
                expect(v1).toBeDefined();
                expect(v1.length).toBe(1);
                expect(v1[0]).toEqual(value1b);
                return [2 /*return*/];
        }
    });
}); });
test("Can post data to service.", function () { return __awaiter(void 0, void 0, void 0, function () {
    var root, rulebase, values, v2name, v2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, new __1.ServiceClient(HOST).init()];
            case 1:
                root = _a.sent();
                return [4 /*yield*/, root.navigate({ service: 'rulebase' })];
            case 2:
                rulebase = _a.sent();
                return [4 /*yield*/, rulebase.navigate({ service: 'values' })];
            case 3:
                values = _a.sent();
                return [4 /*yield*/, values.post(value2)];
            case 4:
                v2name = _a.sent();
                expect(v2name.name).toEqual(value2.name);
                return [4 /*yield*/, values.get(value2.name)];
            case 5:
                v2 = _a.sent();
                expect(v2).toEqual(value2);
                return [2 /*return*/];
        }
    });
}); });
test("Can put data to service.", function () { return __awaiter(void 0, void 0, void 0, function () {
    var root, rulebase, values, v2name, v2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, new __1.ServiceClient(HOST).init()];
            case 1:
                root = _a.sent();
                return [4 /*yield*/, root.navigate({ service: 'rulebase' })];
            case 2:
                rulebase = _a.sent();
                return [4 /*yield*/, rulebase.navigate({ service: 'values' })];
            case 3:
                values = _a.sent();
                return [4 /*yield*/, values.put(value2.name, value3)];
            case 4:
                v2name = _a.sent();
                expect(v2name.name).toEqual(value2.name);
                return [4 /*yield*/, values.get(value2.name)];
            case 5:
                v2 = _a.sent();
                expect(v2).toEqual(value3);
                return [2 /*return*/];
        }
    });
}); });
test("Can patch data to service.", function () { return __awaiter(void 0, void 0, void 0, function () {
    var root, rulebase, values, v2name, v2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, new __1.ServiceClient(HOST).init()];
            case 1:
                root = _a.sent();
                return [4 /*yield*/, root.navigate({ service: 'rulebase' })];
            case 2:
                rulebase = _a.sent();
                return [4 /*yield*/, rulebase.navigate({ service: 'values' })];
            case 3:
                values = _a.sent();
                return [4 /*yield*/, values.patch(value2.name, { type: 'new type' })];
            case 4:
                v2name = _a.sent();
                expect(v2name.name).toEqual(value2.name);
                return [4 /*yield*/, values.get(value2.name)];
            case 5:
                v2 = _a.sent();
                expect(v2).toBeDefined();
                expect(v2.type).toEqual('new type');
                return [2 /*return*/];
        }
    });
}); });
test("Can delete data from service.", function () { return __awaiter(void 0, void 0, void 0, function () {
    var root, rulebase, values, v2name, v2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, new __1.ServiceClient(HOST).init()];
            case 1:
                root = _a.sent();
                return [4 /*yield*/, root.navigate({ service: 'rulebase' })];
            case 2:
                rulebase = _a.sent();
                return [4 /*yield*/, rulebase.navigate({ service: 'values' })];
            case 3:
                values = _a.sent();
                return [4 /*yield*/, values.delete(value2.name)];
            case 4:
                v2name = _a.sent();
                expect(v2name.name).toEqual(value2.name);
                return [4 /*yield*/, values.get(value2.name)];
            case 5:
                v2 = _a.sent();
                expect(v2).toBeFalsy();
                return [2 /*return*/];
        }
    });
}); });
