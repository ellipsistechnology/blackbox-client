"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServiceClient = exports.DataClient = exports.Client = exports.ConnectionError = void 0;
var axios_1 = require("axios");
var url = require("url");
var https = require("https");
var isPathAbsolute = /^\w+:\/\//;
var ConnectionError = /** @class */ (function (_super) {
    __extends(ConnectionError, _super);
    function ConnectionError(_a) {
        var status = _a.status, statusText = _a.statusText, headers = _a.headers, data = _a.data;
        var _this_1 = _super.call(this, status + " " + statusText) || this;
        _this_1.status = status;
        _this_1.statusText = statusText;
        _this_1.headers = headers;
        _this_1.data = data;
        return _this_1;
    }
    return ConnectionError;
}(Error));
exports.ConnectionError = ConnectionError;
var Client = /** @class */ (function () {
    function Client(url, authenticationToken, caBundle) {
        this.authenticationToken = authenticationToken;
        this.caBundle = caBundle;
        this.url = url;
        if (!url.endsWith('/'))
            this.url = this.url + '/';
    }
    // TODO: [BB-1] options()
    Client.prototype.options = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, axios_1.default.options(url, this.requestOptions())];
                    case 1: return [2 /*return*/, (_a.sent())];
                }
            });
        });
    };
    // auth: {
    //     username: 'janedoe',
    //     password: 's00pers3cret'
    //   },
    Client.prototype.requestOptions = function (_a) {
        var _b = _a === void 0 ? {} : _a, verbose = _b.verbose, service = _b.service;
        var opts = {};
        if (this.authenticationToken) {
            opts.headers = {
                Authorization: 'Bearer ' + this.authenticationToken
            };
            if (verbose) {
                opts.params.verbose = true;
            }
            if (service) {
                opts.params.service = true;
            }
        }
        // If on Node then set the HTTPS agent:
        if (https.Agent) {
            if (this.caBundle) {
                console.log("NOTE: CA bundle being used for Blackbox Client (" + this.url + ").");
                opts.httpsAgent = new https.Agent({ ca: this.caBundle });
            }
            else {
                console.log("WARNING: No CA bundle provided for Blackbox Client (" + this.url + "); unauthorised certificates will not be rejected.");
                opts.httpsAgent = new https.Agent({ rejectUnauthorized: false });
            }
        }
        opts.validateStatus = function (_status) { return true; }; // Never reject based on status code
        return opts;
    };
    return Client;
}());
exports.Client = Client;
var DataClient = /** @class */ (function (_super) {
    __extends(DataClient, _super);
    function DataClient(url, authenticationToken, caBundle) {
        return _super.call(this, url, authenticationToken, caBundle) || this;
    }
    /**
     * Ensure the given data can be cast to T.
     * Must throw an Error if validation issues are found.
     */
    DataClient.prototype.validatResponseData = function (_data) {
        // if(!data)
        //   throw new Error(`Empty data received from service at ${this.url}.`)
    };
    DataClient.prototype.service = function (id) {
        var serviceClient = new ServiceClient(url.resolve(this.url, id), this.authenticationToken, this.caBundle);
        return serviceClient.init(); // RESOLVED TODO: [BB-2] pass auth to child service client
        // const response = (await (<any>axios).options(serviceUrl, this.requestOptions()))
        // this.checkStatus(response)
        // if(!response.data)
        //   return null
        // return response.data
    };
    // TODO: [BB-4] Implement a service method that can take a parameter map and fill in {blah} params.
    DataClient.prototype.get = function (id, verbose) {
        if (verbose === void 0) { verbose = false; }
        return __awaiter(this, void 0, void 0, function () {
            var serviceUrl, options, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("TEST");
                        serviceUrl = id ? url.resolve(this.url, id) : this.url;
                        options = this.requestOptions();
                        if (verbose) {
                            options.params = { verbose: '' };
                        }
                        console.log("getting with options: " + JSON.stringify(options));
                        return [4 /*yield*/, axios_1.default.get(serviceUrl, options)];
                    case 1:
                        response = (_a.sent());
                        this.checkStatus(response);
                        if (!response.data)
                            return [2 /*return*/, null];
                        this.validatResponseData(response.data);
                        return [2 /*return*/, response.data];
                }
            });
        });
    };
    DataClient.prototype.query = function (q, verbose) {
        if (verbose === void 0) { verbose = false; }
        return __awaiter(this, void 0, void 0, function () {
            var options, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (typeof q !== 'string') {
                            q = JSON.stringify(q);
                        }
                        options = Object.assign({ params: { q: q } }, this.requestOptions());
                        if (verbose) {
                            options.params.verbose = '';
                        }
                        return [4 /*yield*/, axios_1.default.get(this.url, options)];
                    case 1:
                        response = (_a.sent());
                        this.checkStatus(response);
                        if (!response.data)
                            return [2 /*return*/, null];
                        this.validatResponseData(response.data);
                        return [2 /*return*/, response.data];
                }
            });
        });
    };
    DataClient.prototype.checkStatus = function (response) {
        if (response.status < 200 || response.status >= 300)
            throw new ConnectionError(response);
    };
    DataClient.prototype.post = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, axios_1.default.post(this.url, data, this.requestOptions())];
                    case 1:
                        response = _a.sent();
                        this.checkStatus(response);
                        if (!response.data || !response.data.name)
                            throw new Error(this.url + " gave an invalid response for post, should include a name, but was " + JSON.stringify(response.data));
                        return [2 /*return*/, response.data];
                }
            });
        });
    };
    DataClient.prototype.put = function (id, data) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, axios_1.default.put(url.resolve(this.url, id), data, this.requestOptions())];
                    case 1:
                        response = _a.sent();
                        this.checkStatus(response);
                        if (!response.data || !response.data.name)
                            throw new Error(this.url + " gave an invalid response for put, should include a name, but was " + JSON.stringify(response.data));
                        return [2 /*return*/, response.data];
                }
            });
        });
    };
    DataClient.prototype.patch = function (id, data) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, axios_1.default.patch(url.resolve(this.url, id), data, this.requestOptions())];
                    case 1:
                        response = _a.sent();
                        this.checkStatus(response);
                        if (!response.data || !response.data.name)
                            throw new Error(this.url + " gave an invalid response for patch, should include a name, but was " + JSON.stringify(response.data));
                        return [2 /*return*/, response.data];
                }
            });
        });
    };
    DataClient.prototype.delete = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, axios_1.default.delete(url.resolve(this.url, id), this.requestOptions())];
                    case 1:
                        response = _a.sent();
                        this.checkStatus(response);
                        if (!response.data || !response.data.name)
                            throw new Error(this.url + " gave an invalid response for delete, should include a name, but was " + JSON.stringify(response.data));
                        return [2 /*return*/, response.data];
                }
            });
        });
    };
    return DataClient;
}(Client));
exports.DataClient = DataClient;
var ServiceClient = /** @class */ (function (_super) {
    __extends(ServiceClient, _super);
    function ServiceClient(url, authenticationToken, caBundle) {
        return _super.call(this, url, authenticationToken, caBundle) || this; // URL, no auth token needed, ca bundle if given
    }
    ServiceClient.prototype.validatResponseData = function (data) {
        if (!data)
            throw new Error(this.url + " returned an invalid service: No data.");
        if (data.error && data.error.code)
            throw new Error(this.url + " returned with an error: " + JSON.stringify(data.error));
        if (!data.description)
            throw new Error(this.url + " returned an invalid service: No description.");
        if (!data.bbversion)
            throw new Error(this.url + " returned an invalid service: No bbversion.");
        if (!data.links)
            throw new Error(this.url + " returned an invalid service: No links.");
    };
    ServiceClient.prototype.init = function () {
        var _this_1 = this;
        var _this = this;
        return axios_1.default.get(this.url, this.requestOptions({ service: true }))
            .then(function (_a) {
            var data = _a.data;
            if (!data)
                throw new Error("Empty data received from service at " + _this_1.url + ".");
            _this_1.validatResponseData(data);
            _this.service = data;
            return _this;
        });
    };
    ServiceClient.prototype.allTypes = function () {
        var _this_1 = this;
        if (!this.typeCache) {
            this.typeCache = Object.keys(this.service.links)
                .reduce(function (types, key) {
                if (_this_1.service.links[key].types) {
                    return types.concat(_this_1.service.links[key].types
                        .map(function (type) { return Object.assign(type, { serviceUrl: url.resolve(_this_1.url, _this_1.service.links[key].href) }); }));
                }
                else {
                    return types;
                }
            }, []);
        }
        return this.typeCache;
    };
    ServiceClient.prototype.types = function (service) {
        // Make a list of all types from all child services:
        if (!service) {
            return this.allTypes();
        }
        // Otherwise check that the requested service exists and return its types:
        else if (!this.service.links[service]) {
            throw new Error("Service '" + service + "' not found in " + JSON.stringify(Object.keys(this.service.links)) + ".");
        }
        else {
            return this.service.links[service].types;
        }
    };
    ServiceClient.prototype.typeUris = function () {
        if (!this.typeUriCache) {
            this.typeUriCache = this.types()
                .map(function (service) { return url.resolve(service.uri, service.name); });
        }
        return this.typeUriCache;
    };
    ServiceClient.prototype.matchType = function (type) {
        return this.typeUris().includes(type);
    };
    /**
     * Finds the header by name irrespective of case.
     * @param headers
     * @param name
     */
    ServiceClient.prototype.getHeader = function (headers, name) {
        if (!headers)
            return undefined;
        if (headers[name]) {
            return headers[name];
        }
        else {
            var keys = Object.keys(headers);
            var nameLowerCase = name.toLowerCase();
            for (var index = 0; index < keys.length; index++) {
                if (keys[index].toLowerCase() === nameLowerCase) {
                    return headers[keys[index]];
                }
            }
        }
        return undefined;
    };
    /**
     * Creates either a service or data client for the given url by first requesting its options.
     * @param serviceUrl
     * @param type If a child path has both data and service types, service will be
     *             assumed unless the data type matches this given type.
     */
    ServiceClient.prototype.createClient = function (serviceUrl, type) {
        return __awaiter(this, void 0, void 0, function () {
            var optionsResponse, bbTypeHeader, contentBlackboxTypes;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, axios_1.default.get(serviceUrl, this.requestOptions({ service: true }))
                        // If the blackbox type is a Service use a ServiceClient:
                    ];
                    case 1:
                        optionsResponse = _a.sent();
                        bbTypeHeader = this.getHeader(optionsResponse.headers, 'content-blackbox-type');
                        contentBlackboxTypes = bbTypeHeader ? bbTypeHeader.split(' ') : [];
                        if (!(type && contentBlackboxTypes.includes(type.uri))) return [3 /*break*/, 2];
                        return [2 /*return*/, new DataClient(serviceUrl, this.authenticationToken, this.caBundle)]; // RESOLVED TODO: [BB-2] pass auth to child service client
                    case 2:
                        if (!(contentBlackboxTypes.length === 1 && contentBlackboxTypes.includes('service'))) return [3 /*break*/, 4];
                        return [4 /*yield*/, new ServiceClient(serviceUrl, this.authenticationToken, this.caBundle).init()
                            // } else if(contentBlackboxTypes.includes('service')) { // service AND datatype
                            //   return Object.assign({}, 
                            //     await new ServiceClient(serviceUrl).init(),
                            //     new DataClient(serviceUrl)
                            //   )
                        ]; // RESOLVED TODO: [BB-2] pass auth to child service client
                    case 3: // service type for a service data structure to be returned
                    return [2 /*return*/, _a.sent()
                        // } else if(contentBlackboxTypes.includes('service')) { // service AND datatype
                        //   return Object.assign({}, 
                        //     await new ServiceClient(serviceUrl).init(),
                        //     new DataClient(serviceUrl)
                        //   )
                    ]; // RESOLVED TODO: [BB-2] pass auth to child service client
                    case 4:
                        if (contentBlackboxTypes.length === 0) {
                            console.warn("WARNING: Service did not provide a content-blackbox-type header.");
                        }
                        else if (!this.matchType(bbTypeHeader)) {
                            throw new Error("ERROR: Service responded with content-blackbox-type header '" + bbTypeHeader + "' which is not " +
                                "listed in the parent's service types.");
                        }
                        return [2 /*return*/, new DataClient(serviceUrl, this.authenticationToken, this.caBundle)]; // RESOLVED TODO: [BB-2] pass auth to child service client
                }
            });
        });
    };
    ServiceClient.prototype.navigateByService = function (service, params) {
        return __awaiter(this, void 0, void 0, function () {
            var serviceUrl;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.service.links)
                            throw new Error("Parent service " + this.service + " has no links.");
                        if (!this.service.links[service])
                            throw new Error("Service '" + service + "' not found in " + this.service + ".");
                        serviceUrl = service.match(isPathAbsolute) ?
                            service : url.resolve(this.url, service) // TODO: Should this be using the value in links? How can we handle {name} etc.?
                        ;
                        // Replace name parameters in the URL e.g. /{name}/thing/{username}:
                        if (params) {
                            Object.keys(params)
                                .reduce(function (url, key) { return url.replace("{" + key + "}", params[key]); }, serviceUrl);
                        }
                        return [4 /*yield*/, this.createClient(serviceUrl)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ServiceClient.prototype.navigateByType = function (type) {
        return __awaiter(this, void 0, void 0, function () {
            var typeUrl, _i, _a, t, childClient;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        typeUrl = url.resolve(type.uri, type.name);
                        _i = 0, _a = this.allTypes();
                        _b.label = 1;
                    case 1:
                        if (!(_i < _a.length)) return [3 /*break*/, 4];
                        t = _a[_i];
                        if (!(url.resolve(t.uri, t.name) === typeUrl)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.createClient(t.serviceUrl, type)];
                    case 2:
                        childClient = _b.sent();
                        if (childClient instanceof ServiceClient)
                            return [2 /*return*/, childClient.navigateByType(type)];
                        else
                            return [2 /*return*/, childClient];
                        _b.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4: throw new Error("Service not found for type " + JSON.stringify(type));
                }
            });
        });
    };
    ServiceClient.prototype.navigate = function (_a) {
        var type = _a.type, service = _a.service, params = _a.params;
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!service) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.navigateByService(service, params)];
                    case 1: return [2 /*return*/, _b.sent()];
                    case 2:
                        if (!type) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.navigateByType(type)];
                    case 3: return [2 /*return*/, _b.sent()];
                    case 4: throw new Error("Must provide either a service or a type for navigation.");
                }
            });
        });
    };
    return ServiceClient;
}(Client));
exports.ServiceClient = ServiceClient;
