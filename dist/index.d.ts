import { Service, ServiceType, NamedReference } from 'blackbox-services';
import { AxiosRequestConfig } from 'axios';
interface IConnectionError {
    status: number;
    statusText: string;
    headers: any;
    data: any;
}
export declare class ConnectionError extends Error implements IConnectionError {
    status: number;
    statusText: string;
    headers: any;
    data: any;
    constructor({ status, statusText, headers, data }: IConnectionError);
}
export declare class Client<T> {
    url: string;
    authenticationToken: string;
    caBundle: string;
    constructor(url: string, authenticationToken: string, caBundle: string);
    options(): Promise<any>;
    requestOptions({ verbose, service }?: {
        verbose?: boolean;
        service?: boolean;
    }): AxiosRequestConfig;
}
export declare class DataClient<T> extends Client<T> {
    constructor(url: string, authenticationToken?: string, caBundle?: string);
    /**
     * Ensure the given data can be cast to T.
     * Must throw an Error if validation issues are found.
     */
    validatResponseData(_data: any): void;
    service(id: string): Promise<ServiceClient>;
    get(id?: string, verbose?: boolean): Promise<T | T[] | null>;
    query(q: any, verbose?: boolean): Promise<any>;
    private checkStatus;
    post(data: T): Promise<NamedReference>;
    put(id: string, data: T): Promise<NamedReference>;
    patch(id: string, data: T): Promise<NamedReference>;
    delete(id: string): Promise<NamedReference>;
}
interface ServiceUrl {
    serviceUrl: string;
}
export declare class ServiceClient extends Client<Service> {
    service: Service;
    typeCache: (ServiceType & ServiceUrl)[];
    typeUriCache: string[];
    constructor(url: string, authenticationToken?: string, caBundle?: string);
    validatResponseData(data: any): void;
    init(): Promise<ServiceClient>;
    private allTypes;
    types(service?: string): ServiceType[];
    private typeUris;
    private matchType;
    /**
     * Finds the header by name irrespective of case.
     * @param headers
     * @param name
     */
    private getHeader;
    /**
     * Creates either a service or data client for the given url by first requesting its options.
     * @param serviceUrl
     * @param type If a child path has both data and service types, service will be
     *             assumed unless the data type matches this given type.
     */
    private createClient;
    navigateByService(service: string, params: {
        [key: string]: string;
    }): Promise<Client<any>>;
    navigateByType(type: ServiceType): Promise<Client<any>>;
    navigate({ type, service, params }: {
        type?: ServiceType;
        service?: string;
        params?: {
            [key: string]: string;
        };
    }): Promise<Client<any>>;
}
export {};
