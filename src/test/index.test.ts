import {ServiceClient, Client, DataClient} from '..'
import axios from 'axios'
import { ServiceLinks, Service, ServiceType } from 'blackbox-services'

const HOST = 'http://dummy.com'
const RULEBASE = 'http://dummy.com/rulebase'
const VALUES = 'http://dummy.com/rulebase/values'
const HOST_MISSING_DESCRIPTION = "desc-missing"
const HOST_MISSING_BBVERSION = "bbver-missing"
const HOST_MISSING_LINKS = "links-missing"

// Dummy service data (see __mocks__/axios.js:
const data:any = {}
data[HOST] = data[HOST+'/'] = {
  description: 'blah',
  bbversion: '1.2.3',
  links: {
    self: {
      href: "/"
    },
    rulebase: {
      href: "/rulebase",
      description: "The blackbox rulebase service.",
      types: [
        {
          uri: "http://ellipsistechnology.com/schemas/blackbox/",
          name: "rule"
        },
        {
          uri: "http://ellipsistechnology.com/schemas/blackbox/",
          name: "condition"
        },
        {
          uri: "http://ellipsistechnology.com/schemas/blackbox/",
          name: "value"
        }
      ]
    },
    test: {
      href: "/test",
      description: "Test.",
      types: [
        {
          uri: "http://ellipsistechnology.com/schemas/blackbox/",
          name: "test"
        }
      ]
    }
  }
}
data[RULEBASE] = data[RULEBASE+'/'] = {
  "description": "Blackbox rule-base service.",
  "bbversion": "1.0.0",
  "links": {
    "self": {
      "href": "/"
    },
    "rules": {
      "href": "/rulebase/rules",
      "description": "",
      "types": [
        {
          "uri": "http://ellipsistechnology.com/schemas/blackbox/",
          "name": "rule",
          "format": "openapi:3.0:Schema Object"
        }
      ]
    },
    "conditions": {
      "href": "/rulebase/conditions",
      "description": "",
      "types": [
        {
          "uri": "http://ellipsistechnology.com/schemas/blackbox/",
          "name": "condition",
          "format": "openapi:3.0:Schema Object"
        }
      ]
    },
    "values": {
      "href": "/rulebase/values",
      "description": "",
      "types": [
        {
          "uri": "http://ellipsistechnology.com/schemas/blackbox/",
          "name": "value",
          "format": "openapi:3.0:Schema Object"
        }
      ]
    }
  }
}
const value1a = {
  name: 'bob',
  type: 'remote'
}
const value1b = {
  name: 'bill',
  type: 'remote'
}
const value2 = {
  name: 'bob',
  type: 'remote'
}
const value3 = {
  name: 'bob',
  type: 'some other type'
}
data[VALUES] = data[VALUES+'/'] = [value1a, value1b]
data[VALUES+'/'+value1a.name] = value1a
data[VALUES+'/'+value1b.name] = value1b
data[HOST_MISSING_LINKS] = {
  description: 'blah',
  bbversion: '1.2.3'
}
data[HOST_MISSING_DESCRIPTION] = {
  bbversion: '1.2.3',
  links: {}
}
data[HOST_MISSING_BBVERSION] = {
  description: 'blah',
  links: {}
};
const optionsData = {}
optionsData[HOST] = optionsData[HOST+'/'] = {

};
const optionsHeaders = {}
optionsHeaders[HOST] = optionsHeaders[HOST+'/'] = {
  'Content-Blackbox-Type': 'service'
};
optionsHeaders[RULEBASE] = optionsHeaders[RULEBASE+'/'] = {
  'Content-Blackbox-Type': 'service'
};
optionsHeaders[VALUES] = optionsHeaders[VALUES+'/'] = {
  'Content-Blackbox-Type': 'http://ellipsistechnology.com/schemas/blackbox/value'
};
(<any>axios).__setup__(data, optionsData, optionsHeaders)

test("Invalid host response throws error.", async () => {
  await expect(new ServiceClient(`doesn't exist`).init()).rejects.toBeTruthy()
  await expect(new ServiceClient(HOST_MISSING_LINKS).init()).rejects.toBeTruthy()
  await expect(new ServiceClient(HOST_MISSING_DESCRIPTION).init()).rejects.toBeTruthy()
  await expect(new ServiceClient(HOST_MISSING_BBVERSION).init()).rejects.toBeTruthy()
})

test("Root node returns a service.", async () => {
  const client = await new ServiceClient(HOST).init()
  expect(client.url).toEqual(HOST+'/')
  expect(client.service).toEqual(data[HOST])
})

// TODO test services linked by absolute URL
test("Can navigate to child service.", async () => {
  const client = await new ServiceClient(HOST).init()

  const ruleBaseTypes:ServiceType[] = client.types('rulebase')
  expect(ruleBaseTypes).toEqual(data[HOST].links.rulebase.types)

  const allTypes:ServiceType[] = client.types()
  expect(allTypes).toEqual(data[HOST].links.rulebase.types.concat(data[HOST].links.test.types))

  await expect(
    client.navigate({service: 'rulebase'})
    .then((rulebaseClient:ServiceClient) => {
      expect(rulebaseClient.service).toEqual(data[RULEBASE])
      return client.navigate({type: {
        uri: 'http://ellipsistechnology.com/schemas/blackbox/',
        name: 'value',
        format: ''
      }})
    })
    .then((valuesClient: DataClient<any>) => valuesClient.get())
    .then(response => expect(response).toEqual(data[VALUES]))
  ).resolves.toBeUndefined()
})

test("Can get data from service by name.", async () => {
  const root = await new ServiceClient(HOST).init()
  const rulebase = await root.navigate({service: 'rulebase'})
  expect(rulebase).toBeInstanceOf(ServiceClient);
  const values = await (<ServiceClient>rulebase).navigate({service: 'values'})
  expect(values).toBeInstanceOf(DataClient);

  const v1 = await (<DataClient<any>>values).get(value1a.name)
  expect(v1).toEqual(value1a)
})

test("Can get data with query.", async () => {
  const root = await new ServiceClient(HOST).init()
  const rulebase = await root.navigate({service: 'rulebase'})
  expect(rulebase).toBeInstanceOf(ServiceClient);
  const values = await (<ServiceClient>rulebase).navigate({service: 'values'})
  expect(values).toBeInstanceOf(DataClient);

  const v1 = await (<DataClient<any>>values).query("bill")
  expect(v1).toBeDefined()
  expect(v1.length).toBe(1)
  expect(v1[0]).toEqual(value1b)
})

test("Can post data to service.", async () => {
  const root = await new ServiceClient(HOST).init()
  const rulebase = await root.navigate({service: 'rulebase'})
  const values = await (<ServiceClient>rulebase).navigate({service: 'values'})

  const v2name = await (<DataClient<any>>values).post(value2)
  expect(v2name.name).toEqual(value2.name)

  const v2 = await (<DataClient<any>>values).get(value2.name)
  expect(v2).toEqual(value2)
})

test("Can put data to service.", async () => {
  const root = await new ServiceClient(HOST).init()
  const rulebase = await root.navigate({service: 'rulebase'})
  const values = await (<ServiceClient>rulebase).navigate({service: 'values'})

  const v2name = await (<DataClient<any>>values).put(value2.name, value3)
  expect(v2name.name).toEqual(value2.name)

  const v2 = await (<DataClient<any>>values).get(value2.name)
  expect(v2).toEqual(value3)
})

test("Can patch data to service.", async () => {
  const root = await new ServiceClient(HOST).init()
  const rulebase = await root.navigate({service: 'rulebase'})
  const values = await (<ServiceClient>rulebase).navigate({service: 'values'})

  const v2name = await (<DataClient<any>>values).patch(value2.name, {type: 'new type'})
  expect(v2name.name).toEqual(value2.name)

  const v2 = await (<DataClient<any>>values).get(value2.name)
  expect(v2).toBeDefined()
  expect(v2.type).toEqual('new type')
})

test("Can delete data from service.", async () => {
  const root = await new ServiceClient(HOST).init()
  const rulebase = await root.navigate({service: 'rulebase'})
  const values = await (<ServiceClient>rulebase).navigate({service: 'values'})

  const v2name = await (<DataClient<any>>values).delete(value2.name)
  expect(v2name.name).toEqual(value2.name)

  const v2 = await (<DataClient<any>>values).get(value2.name)
  expect(v2).toBeFalsy()
})
