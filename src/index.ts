import {Service, ServiceType, NamedReference} from 'blackbox-services'
import axios, { AxiosRequestConfig } from 'axios'
import * as url from 'url'
import * as https from 'https'

const isPathAbsolute = /^\w+:\/\//

interface IConnectionError {
  status: number
  statusText: string
  headers: any
  data: any
}
export class ConnectionError extends Error implements IConnectionError {
  status: number
  statusText: string
  headers: any
  data: any

  constructor({status, statusText, headers, data}: IConnectionError) {
    super(`${status} ${statusText}`)
    this.status = status
    this.statusText = statusText
    this.headers = headers
    this.data = data
  }
}

export class Client<T> {
  url: string
  authenticationToken: string
  caBundle: string

  constructor(url: string, authenticationToken: string, caBundle: string) {
    this.authenticationToken = authenticationToken
    this.caBundle = caBundle
    this.url = url
    if(!url.endsWith('/'))
      this.url = this.url + '/'
  }

  // TODO: [BB-1] options()
  async options():Promise<any> {
    return (await (<any>axios).options(url, this.requestOptions()))
  }

  // auth: {
  //     username: 'janedoe',
  //     password: 's00pers3cret'
  //   },
  requestOptions({verbose, service}: {verbose?: boolean, service?: boolean} = {}) {
    const opts: AxiosRequestConfig = {}
    
    if(this.authenticationToken) {
      opts.headers = {
        Authorization: 'Bearer '+this.authenticationToken
      }
      if(verbose) {
        opts.params.verbose = true
      }
      if(service) {
        opts.params.service = true
      }
    }

    // If on Node then set the HTTPS agent:
    if(https.Agent) {
      if(this.caBundle) {
        console.log(`NOTE: CA bundle being used for Blackbox Client (${this.url}).`);
        opts.httpsAgent = new https.Agent({ ca: this.caBundle })
      } else {
        console.log(`WARNING: No CA bundle provided for Blackbox Client (${this.url}); unauthorised certificates will not be rejected.`);
        opts.httpsAgent = new https.Agent({rejectUnauthorized: false})
      }
    }

    opts.validateStatus = (_status) => true // Never reject based on status code
    return opts
  }
}

export class DataClient<T> extends Client<T> {
  constructor(url: string, authenticationToken?: string, caBundle?: string) {
    super(url, authenticationToken, caBundle)
  }

  /**
   * Ensure the given data can be cast to T.
   * Must throw an Error if validation issues are found.
   */
  validatResponseData(_data: any) {
    // if(!data)
    //   throw new Error(`Empty data received from service at ${this.url}.`)
  }

  service(id: string): Promise<ServiceClient> {
    const serviceClient = new ServiceClient(url.resolve(this.url, id), this.authenticationToken, this.caBundle)
    return serviceClient.init() // RESOLVED TODO: [BB-2] pass auth to child service client
    // const response = (await (<any>axios).options(serviceUrl, this.requestOptions()))

    // this.checkStatus(response)
    
    // if(!response.data)
    //   return null

    // return response.data
  }

  // TODO: [BB-4] Implement a service method that can take a parameter map and fill in {blah} params.

  async get(id?:string, verbose = false):Promise<T|T[]|null> {
    const serviceUrl = id ? url.resolve(this.url, id) : this.url
    const options = this.requestOptions()
    if(verbose) {
      options.params = {verbose: ''}
    }
    const response = (await axios.get(serviceUrl, options))

    this.checkStatus(response)

    if(!response.data)
      return null

    this.validatResponseData(response.data)
    return response.data
  }

  async query(q: any, verbose = false) {
    if(typeof q !== 'string') {
      q = JSON.stringify(q)
    }

    const options = Object.assign({params: {q}}, this.requestOptions())
    if(verbose) {
      options.params.verbose = ''
    }
    
    const response = (await axios.get(this.url, options))

    this.checkStatus(response)

    if(!response.data)
      return null

    this.validatResponseData(response.data)
    return response.data
  }

  private checkStatus(response) {
    if(response.status < 200 || response.status >= 300)
      throw new ConnectionError(response)
  }

  async post(data:T):Promise<NamedReference> {
    const response = await axios.post(this.url, data, this.requestOptions())

    this.checkStatus(response)

    if(!response.data || !response.data.name)
      throw new Error(`${this.url} gave an invalid response for post, should include a name, but was ${JSON.stringify(response.data)}`)
    return response.data
  }

  async put(id:string, data:T):Promise<NamedReference> {
    const response = await axios.put(url.resolve(this.url, id), data, this.requestOptions())

    this.checkStatus(response)

    if(!response.data || !response.data.name)
      throw new Error(`${this.url} gave an invalid response for put, should include a name, but was ${JSON.stringify(response.data)}`)
    return response.data
  }

  async patch(id:string, data:T):Promise<NamedReference> {
    const response = await axios.patch(url.resolve(this.url, id), data, this.requestOptions())

    this.checkStatus(response)

    if(!response.data || !response.data.name)
      throw new Error(`${this.url} gave an invalid response for patch, should include a name, but was ${JSON.stringify(response.data)}`)
    return response.data
  }

  async delete(id:string):Promise<NamedReference> {
    const response = await axios.delete(url.resolve(this.url, id), this.requestOptions())

    this.checkStatus(response)

    if(!response.data || !response.data.name)
      throw new Error(`${this.url} gave an invalid response for delete, should include a name, but was ${JSON.stringify(response.data)}`)
    return response.data
  }
}

interface ServiceUrl { serviceUrl: string }

export class ServiceClient extends Client<Service> {
  service:Service
  typeCache:(ServiceType&ServiceUrl)[]
  typeUriCache:string[]

  constructor(url:string, authenticationToken?: string, caBundle?: string) {
    super(url, authenticationToken, caBundle) // URL, no auth token needed, ca bundle if given
  }

  validatResponseData(data:any) {
    if(!data)
      throw new Error(`${this.url} returned an invalid service: No data.`)
    if(data.error && data.error.code)
      throw new Error(`${this.url} returned with an error: ${JSON.stringify(data.error)}`)
    if(!data.description)
      throw new Error(`${this.url} returned an invalid service: No description.`)
    if(!data.bbversion)
      throw new Error(`${this.url} returned an invalid service: No bbversion.`)
    if(!data.links)
      throw new Error(`${this.url} returned an invalid service: No links.`)
  }

  init(): Promise<ServiceClient> {
    const _this = this
    return axios.get(this.url, this.requestOptions({service: true}))
    .then(({data}) => {
      if(!data)
        throw new Error(`Empty data received from service at ${this.url}.`)
      this.validatResponseData(data)
      _this.service = data
      return _this
    })
  }

  private allTypes():(ServiceType&ServiceUrl)[] {
    if(!this.typeCache) {
      this.typeCache = Object.keys(this.service.links)
        .reduce((types:(ServiceType&ServiceUrl)[], key:string) => {
          if(this.service.links[key].types) {
            return types.concat(this.service.links[key].types
              .map((type) => Object.assign(type, {serviceUrl: url.resolve(this.url, this.service.links[key].href)})))
          }
          else {
            return types
          }
        }, [])
    }
    return this.typeCache
  }

  types(service?:string):ServiceType[] {
    // Make a list of all types from all child services:
    if(!service) {
      return this.allTypes()
    }
    // Otherwise check that the requested service exists and return its types:
    else if(!this.service.links[service]) {
      throw new Error(`Service '${service}' not found in ${JSON.stringify(Object.keys(this.service.links))}.`)
    }
    else {
      return this.service.links[service].types
    }
  }

  private typeUris() {
    if(!this.typeUriCache) {
      this.typeUriCache = this.types()
        .map((service:ServiceType) => url.resolve(service.uri, service.name))
    }
    return this.typeUriCache
  }

  private matchType(type: string):boolean {
    return this.typeUris().includes(type)
  }

  /**
   * Finds the header by name irrespective of case.
   * @param headers 
   * @param name 
   */
  private getHeader(headers: any, name: string) {
    if(!headers)
      return undefined
    if(headers[name]) {
      return headers[name]
    } else {
      const keys = Object.keys(headers)
      const nameLowerCase = name.toLowerCase()
      for (let index = 0; index < keys.length; index++) {
        if(keys[index].toLowerCase() === nameLowerCase) {
          return headers[keys[index]]
        }
      }
    }
    return undefined
  }

  /**
   * Creates either a service or data client for the given url by first requesting its options.
   * @param serviceUrl 
   * @param type If a child path has both data and service types, service will be 
   *             assumed unless the data type matches this given type.
   */
  private async createClient(serviceUrl: string, type?: ServiceType): Promise<Client<any>> {
    const optionsResponse = await axios.get(serviceUrl, this.requestOptions({service: true}))

    // If the blackbox type is a Service use a ServiceClient:
    const bbTypeHeader = this.getHeader(optionsResponse.headers, 'content-blackbox-type')
    const contentBlackboxTypes: string[] = bbTypeHeader ? bbTypeHeader.split(' ') : []
    
    if(type && contentBlackboxTypes.includes(type.uri)) {
      return new DataClient<any>(serviceUrl, this.authenticationToken, this.caBundle) // RESOLVED TODO: [BB-2] pass auth to child service client
    
    } else if(contentBlackboxTypes.length === 1 && contentBlackboxTypes.includes('service')) { // service type for a service data structure to be returned
      return await new ServiceClient(serviceUrl, this.authenticationToken, this.caBundle).init() // RESOLVED TODO: [BB-2] pass auth to child service client

    // } else if(contentBlackboxTypes.includes('service')) { // service AND datatype
    //   return Object.assign({}, 
    //     await new ServiceClient(serviceUrl).init(),
    //     new DataClient(serviceUrl)
    //   )

    } else { // Otherwise assume a DataClient (i.e. not a parent service/service wrapper):
      if(contentBlackboxTypes.length === 0) {
        console.warn(`WARNING: Service did not provide a content-blackbox-type header.`)
      }
      else if(!this.matchType(bbTypeHeader)) {
        throw new Error(
          `ERROR: Service responded with content-blackbox-type header '${bbTypeHeader}' which is not `+
          `listed in the parent's service types.`
        )
      }
      return new DataClient<any>(serviceUrl, this.authenticationToken, this.caBundle) // RESOLVED TODO: [BB-2] pass auth to child service client
    }
  }

  async navigateByService(service:string, params: {[key: string]: string}):Promise<Client<any>> {
    if(!this.service.links)
      throw new Error(`Parent service ${this.service} has no links.`)
    if(!this.service.links[service])
      throw new Error(`Service '${service}' not found in ${this.service}.`)

    const serviceUrl = service.match(isPathAbsolute) ?
      service : url.resolve(this.url, service) // TODO: Should this be using the value in links? How can we handle {name} etc.?

    // Replace name parameters in the URL e.g. /{name}/thing/{username}:
    if(params) {
      Object.keys(params)
      .reduce((url, key) => url.replace(`{${key}}`, params[key]), serviceUrl)
    }

    return await this.createClient(serviceUrl)
  }

  async navigateByType(type:ServiceType):Promise<Client<any>> {
    const typeUrl = url.resolve(type.uri, type.name)
    for(let t of this.allTypes()) {
      if(url.resolve(t.uri, t.name) === typeUrl) {
        const childClient = await this.createClient(t.serviceUrl, type)
        if(childClient instanceof ServiceClient)
          return (<ServiceClient>childClient).navigateByType(type)
        else
          return childClient
      }
    }
    throw new Error(`Service not found for type ${JSON.stringify(type)}`)
  }

  async navigate({type, service, params}: {type?:ServiceType, service?:string, params?: {[key: string]: string}}): Promise<Client<any>> {
    if(service) {
      return await this.navigateByService(service, params)
    }
    else if(type) {
      return await this.navigateByType(type)
    }
    else {
      throw new Error(`Must provide either a service or a type for navigation.`)
    }
  }
}
